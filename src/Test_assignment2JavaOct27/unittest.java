/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Test_assignment2JavaOct27;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import assignment2JavaOct27.*;
import java.sql.SQLException;
import java.sql.Timestamp;
/**
 *
 * @author cha_xi
 */

public class unittest {
    private MedBean medB;
    private Patient patient;
    private InPatientBean inPatientB;
    private SurgicalBean surgicalB;
    private PatientDAOImpl pDAOImpl;
    
    @Before
    public void unittest(){
        Timestamp dateOfMed = Timestamp.valueOf("2000-2-2 1:1:1");
        Timestamp admissionDate = Timestamp.valueOf("1999-3-3 23:59:59");
        Timestamp releaseDate = Timestamp.valueOf("2001-4-4 2:2:2");
        Timestamp dateOfStay = Timestamp.valueOf("2002-5-5 8:8:8");
        Timestamp dateOfSurgery = Timestamp.valueOf("2009-6-6 9:9:9");
        
        medB = new MedBean(-1, -2, dateOfMed, "medtest", 0.1, 0.2);
        patient = new Patient(-11, "LastNameTest", "FirstNameTest", "TestDiagnosis", admissionDate, releaseDate);
        inPatientB = new InPatientBean(-22, -33, dateOfStay, "AAA2002BB", 0.01, 1.01, 2.33);
        surgicalB = new SurgicalBean(-111, -222, dateOfSurgery, "surgery", 100.1, 10.1, 1.99);
        
        pDAOImpl = new PatientDAOImpl();
    }
    
    @Test
    public void insertTest_Patient() throws SQLException{
    
        System.out.println("find 1st patient:/n"+pDAOImpl.findById(1).toString());
        //System.out.println("insert Patient:"+patient.toString()+"\n result: "+
          //      pDAOImpl.insertPatient(patient));
        //System.out.println("find Patient:"+pDAOImpl.findById(patient.getPatientID()).toString());
        
    }
    
}
