/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2javaoct27;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *this class is for the table inpatient
 * @author cha_xi
 */
public class InPatientBean {
    private int id;
    private int patientId;
    private Timestamp dateOfStay;
    private String roomNumber;
    private double dailyRate;
    private double supplies;
    private double services;

    public InPatientBean(int id, int patientId, Timestamp dateOfStay, String roomNumber, double dailyRate, double supplies, double services) {
        this.id = id;
        this.patientId = patientId;
        this.dateOfStay = dateOfStay;
        this.roomNumber = roomNumber;
        this.dailyRate = dailyRate;
        this.supplies = supplies;
        this.services = services;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public Timestamp getDateOfStay() {
        return dateOfStay;
    }

    public void setDateOfStay(Timestamp dateOfStay) {
        this.dateOfStay = dateOfStay;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public double getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(double dailyRate) {
        this.dailyRate = dailyRate;
    }

    public double getSupplies() {
        return supplies;
    }

    public void setSupplies(double supplies) {
        this.supplies = supplies;
    }

    public double getServices() {
        return services;
    }

    public void setServices(double services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "InPatientBean{" + "id=" + id + ", patientId=" + patientId + ", dateOfStay=" + dateOfStay + ", roomNumber=" + roomNumber + ", dailyRate=" + dailyRate + ", supplies=" + supplies + ", services=" + services + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.id;
        hash = 83 * hash + this.patientId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InPatientBean other = (InPatientBean) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.patientId != other.patientId) {
            return false;
        }
        if (!Objects.equals(this.dateOfStay, other.dateOfStay)) {
            return false;
        }
        if (!Objects.equals(this.roomNumber, other.roomNumber)) {
            return false;
        }
        if (Double.doubleToLongBits(this.dailyRate) != Double.doubleToLongBits(other.dailyRate)) {
            return false;
        }
        if (Double.doubleToLongBits(this.supplies) != Double.doubleToLongBits(other.supplies)) {
            return false;
        }
        if (Double.doubleToLongBits(this.services) != Double.doubleToLongBits(other.services)) {
            return false;
        }
        return true;
    }

  

   

   
    
    
}
