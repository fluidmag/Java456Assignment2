/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2javaoct27;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author cha_xi
 */
public interface PatientDAO {
    public int deleteMed(int id) throws SQLException;
        
    public int deleteSur(int id) throws SQLException;
    public int deleteInp(int id) throws SQLException;
    
    public int insertMed(MedBean b) throws SQLException;
    public int insertSur(SurgicalBean b) throws SQLException;
    public int insertInp(InPatientBean b) throws SQLException;
    
    public int updateMed(MedBean b)throws SQLException;
    public String dateSur(SurgicalBean b)throws SQLException;
    public int updateInp(InPatientBean b)throws SQLException;
    
    public Patient findById(int key) throws SQLException;
    public ArrayList<Patient> findAll() throws SQLException;
    public int updatePatient(Patient p)throws SQLException;
    public int insertPatient(Patient p)throws SQLException;
    public int deletePatient(Patient p)throws SQLException;
}
